//
//  ViewController.swift
//  MC3
//
//  Created by Christian Varriale on 17/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UITableViewDelegate, LocationServiceDelegate {
    
    ///Definizione dell'istanza del Singleton e del dataSource relativa alla TableView di tipo Diffable (verrà usata per il caricamento del contenuto)
    //MARK: - Properties
    var dataSource: UITableViewDiffableDataSource<Int, Ristorante>?
    let locationSingleton = LocationSingleton.sharedInstance
    
    ///Proprietà per il Messaggio di Alert che ha bisogno del timer, del tempo rimanente e del messaggio che poi verrà mostrato
    var alertController: UIAlertController?
    var alertTimer: Timer?
    var remainingTime = 0
    var baseMessage: String?
    
    //MARK: - IBOutlet
    @IBOutlet weak var tableView : UITableView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationSingleton.delegate = self
        locationSingleton.startUpdatingLocation()
        
        dataSource = restaurantDataSource()
        
        ///Definizione del refresh control legato allo scrolling con richiamo della funzione refresh che effettuerà la query
        tableView.dataSource = dataSource
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        refresh()
        
//        self.showAlertMsg(title: "Attention", message: "The restaurants have been selected in such a way as to guarantee users the maximum possible safety, but nevertheless not being present in the kitchens, we do not take responsibility for any bad experiences. ", time: 15)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadSnapshot(animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// CHIEDERE A MARCO - Funzione richiamata quando la view si è caricata; dovrebbe caricare nella tableView i riferimenti ottenuti dalla query sul DB in base alla posizione
    //MARK: - Function
    @objc func refresh() {
        
        Model.currentModel.refresh(LocationSingleton.sharedInstance.lastLocation) { error in
            if let error = error {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.tableView.refreshControl?.endRefreshing()
                return
            }
            self.tableView.refreshControl?.endRefreshing()
            self.reloadSnapshot(animated: true)
        }
    }
    
    //MARK: - Function For Alert
    func showAlertMsg(title: String, message: String, time: Int) {
        
        guard (self.alertController == nil) else {
            print("Alert already displayed")
            return
        }
        
        self.baseMessage = message
        self.remainingTime = time
        
        self.alertController = UIAlertController(title: title, message: self.alertMessage(), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Alert was cancelled")
            self.alertController=nil;
            self.alertTimer?.invalidate()
            self.alertTimer=nil
        }
        
        self.alertController!.addAction(cancelAction)
        
        self.alertTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ViewController.countDown), userInfo: nil, repeats: true)
        
        self.present(self.alertController!, animated: true, completion: nil)
    }
    
    @objc func countDown() {
        
        self.remainingTime -= 1
        if (self.remainingTime < 0) {
            self.alertTimer?.invalidate()
            self.alertTimer = nil
            self.alertController!.dismiss(animated: true, completion: {
                self.alertController = nil
            })
        } else {
            self.alertController!.message = self.alertMessage()
        }
        
    }
    
    func alertMessage() -> String {
        var message=""
        if let baseMessage=self.baseMessage {
            message=baseMessage+" "
        }
        //        return(message+"\(self.remainingTime)")
        return(message)
    }
    
    //MARK: - IBAction
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "FilterView", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FilterView") as! FilterViewController
        
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    ///Funzione richiamata nel momento in cui apro i filtri, li applico, in base alle scelte applico updateFilter() con conseguente ReloadSnapshot
    @IBAction func unwindToViewControllerA(segue: UIStoryboardSegue) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                Model.currentModel.updateFilter()
                self.reloadSnapshot(animated: false)
            }
        }
    }
    
    @IBAction func favoritesButtonPressed(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "FavoritesView", bundle: nil)
        let favoritesViewController = storyBoard.instantiateViewController(withIdentifier: "Favorites") as! FavoritesViewController
        
        self.navigationController?.pushViewController(favoritesViewController, animated: true)
    }
    
    //MARK: - Implementation Protocol
    func locationDidUpdateToLocation(currentLocation: CLLocation)
    {
        //        print(LocationSingleton.sharedInstance.lastLocation.coordinate.latitude)
        //        print(LocationSingleton.sharedInstance.lastLocation.coordinate.longitude)
    }
    
    func locationUpdateDidFailWithError(error: NSError)
    {
        //        print(error)
    }
}

//MARK: - Extension View Controller for Push Restaurant to Detail View
extension ViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DetailView", bundle: nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier : "Detail") as! DetailViewController
        
        let cell = tableView.cellForRow(at: indexPath) as? RestaurantCell
        detailViewController.ristorante = cell?.restaurant
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension ViewController {
    //MARK: - Cell Table Definition
    private func restaurantDataSource() -> UITableViewDiffableDataSource<Int, Ristorante> {
        let reuseIdentifier = "RestaurantCell"
        return UITableViewDiffableDataSource(tableView: tableView) { (tableView, indexPath, restaurant) -> RestaurantCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? RestaurantCell
            cell?.restaurant = restaurant
            
            // Restaurant is favourite? -> fillHeart
            if CoreDataController.sharedIstance.getFavouriteRestaurant(idRestaurant: restaurant.name) == nil {
                cell?.favouriteCellButton!.isSelected = false
            }else {
                cell?.favouriteCellButton!.isSelected = true
            }
            
            return cell
        }
    }
    
    //MARK: - Reload the overlay for table view
    func reloadSnapshot(animated: Bool) {
        var snapshot = NSDiffableDataSourceSnapshot<Int, Ristorante>()
        
        snapshot.appendSections([0])
        snapshot.appendItems(Model.currentModel.getArray())
        
        dataSource?.apply(snapshot, animatingDifferences: animated)
        
        if Model.currentModel.getArray().isEmpty {
            let label = UILabel()
            label.text = "No Restaurants Found"
            label.textColor = UIColor.systemGray2
            label.textAlignment = .center
            label.font = UIFont.preferredFont(forTextStyle: .title2)
            tableView.backgroundView = label
            tableView.backgroundColor = UIColor.white
        } else {
            tableView.backgroundColor = UIColor.white
        }
    }
}
