//
//  UIViewAnimations.swift
//  FiltersView
//
//  Created by Stefano Di Nunno on 17/02/2020.
//  Copyright © 2020 Stefano Di Nunno. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Definition Animation
extension UIView {
    
    func showVertically() {
        let vertically = CABasicAnimation(keyPath: "position")
        vertically.keyPath = "position"
        vertically.duration = 2
        
        layer.add(vertically, forKey: "position")
    }
    
    func showVerticallyDown() {
        let vertically = CABasicAnimation(keyPath: "position")
        vertically.keyPath = "position"
        vertically.duration = 0.2
        vertically.speed = 0.8
        
        self.frame.origin.y += 554
        
        layer.add(vertically, forKey: "position")
    }
    
}
