//
//  RestaurantCell.swift
//  MC3
//
//  Created by Stefano Di Nunno on 18/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Class Definition
class RestaurantCell: UITableViewCell {
    
    ///Contenuto della cella, collegato alle relative IBOutlet
    //MARK: - IBOutlet
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var address: UILabel?
    @IBOutlet weak var distance: UILabel?
    @IBOutlet weak var favouriteCellButton: UIButton?
    
    ///Settaggio della variabile restaurant in base agli outlet superiori. La distance si ottiene con la funzione definita nel Ristorante.swift che calcola la distanza tra la tua posizione salvata nella last location del Singleton e quella fissa del locale
    //MARK: - Properties
    var restaurant: Ristorante? {
        
        didSet {
            name!.text = restaurant?.name
            address!.text = restaurant?.address
            distance!.text = String(format: "%.2f Km", Double((LocationSingleton.sharedInstance.lastLocation.distance(from: restaurant!.location)))/1000.0)
            imgView?.image = UIImage(named: restaurant!.cardPhoto)
        }
        
    }
    
    ///funzione, che prende il parametro ristorante, e verrà usata per settare ogni cella nel richiamo della funzione della tableView
    func configure (restaurant : Ristorante) {
        
        self.name!.text = restaurant.name
        self.address!.text = restaurant.address
        self.distance!.text = String(format: "%.2f Km", Double((LocationSingleton.sharedInstance.lastLocation.distance(from: restaurant.location)))/1000.0)
        self.imgView?.image = UIImage(named: restaurant.cardPhoto)
        
    }
    
    
    //MARK: - Cell Function
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    ///Con questa funzione se tocco un cuore il (sender.tag.selected = true) allora lo tolgo dai preferiti, altrimenti lo aggiungo
    @IBAction func favouriteCellButtonPressed(_ sender: UIButton) {
        
        if sender.isSelected {
            // remove
            CoreDataController.sharedIstance.deleteFavouriteRestaurant(idRestaurant: restaurant!.name)
        }else {
            // add
            CoreDataController.sharedIstance.addFavouriteRestaurant(idRestaurant: restaurant!.name)
        }
        
        sender.isSelected = !sender.isSelected
        
    }
}
