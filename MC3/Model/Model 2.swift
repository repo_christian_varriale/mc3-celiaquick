//
//  Model.swift
//  MC3
//
//  Created by Marco Longobardi on 29/02/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import CloudKit

//MARK: - Class Definition
class Model {
    
    // MARK: - iCloud Info
    let container: CKContainer
    let publicDB: CKDatabase
    
    // MARK: - Properties
    private(set) var ristoranti: [Ristorante] = []
    private(set) var singleRist: [Ristorante] = []
    static var currentModel = Model()
    
    private var areaFilter: String = ""
    private var categoryFilter: String = ""
    
    //MARK: - Constructor
    init() {
        container = CKContainer.default()
        publicDB = container.publicCloudDatabase
        
    }
    
    //MARK: - Query Function to Retrieve Data
    
    func getRestaurant(nameRestaurant: String,  _ completion: @escaping (Error?) -> Void) {
//        print(nameRestaurant)
        let predicate = NSPredicate(format: "name == %@", nameRestaurant)
        let query = CKQuery(recordType: "Restaurant", predicate: predicate)
        
        publicDB.perform(query, inZoneWith: CKRecordZone.default().zoneID){ [weak self] results, error in
            guard let self = self else {return}
            if let error = error {
                DispatchQueue.main.async {
                    completion(error)
                }
                return
            }
            guard let results = results else {return}
            self.singleRist = results.compactMap {
                Ristorante(record: $0, database: self.publicDB)
            }
//            print(self.singleRist.first?.name)
            DispatchQueue.main.async{
                completion(nil)
            }
            
        }
        
//        publicDB.perform(query, inZoneWith: CKRecordZone.default().zoneID){ (record,error) in
//            if let record = record {
//               let ristorante = record.compactMap {
//                    Ristorante(record: $0, database: self.publicDB)
//                }.first!
//                ret = ristorante
//                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@")
//                print(ret?.name)
//            }
//        }
//        print("[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]")
//        print(ret?.name)
//        return ret
    }
    
    @objc func refresh (_ userLoc: CLLocation , _ completion: @escaping (Error?) -> Void) {
        
        //    let variabile_temporanea = "King Wok"
        //    40.71 14.50
        //        let coordinate = CLLocation(latitude: 40.71, longitude: 14.50)
        //    let predicate = NSPredicate(format: "name == %@ AND altro == %@", variabile_temporanea, "ao")
        
        var predicate = NSPredicate(value: true)
        
        if (self.areaFilter != "" && self.categoryFilter != ""){
            predicate = NSPredicate(format: "naplesArea == %@ AND category == %@", self.areaFilter, self.categoryFilter)
        }
        else if (self.areaFilter != "" && self.categoryFilter == ""){
            predicate = NSPredicate(format: "naplesArea == %@", self.areaFilter)
        }
        else if (self.areaFilter == "" && self.categoryFilter != ""){
            predicate = NSPredicate(format: "category == %@", self.categoryFilter)
        }
        
        let query = CKQuery(recordType: "Restaurant", predicate: predicate)
        
        query.sortDescriptors = [CKLocationSortDescriptor(key: "location", relativeLocation: LocationSingleton.sharedInstance.lastLocation)]
        
        ristoranti(forQuery: query, completion)
    }
    
    //MARK: - Connection DB CloudKit
    private func ristoranti(forQuery query: CKQuery, _ completion: @escaping (Error?) -> Void) {
        
        publicDB.perform(query, inZoneWith: CKRecordZone.default().zoneID){ [weak self] results, error in
            guard let self = self else {return}
            if let error = error {
                DispatchQueue.main.async {
                    completion(error)
                }
                return
            }
            guard let results = results else {return}
            self.ristoranti = results.compactMap {
                Ristorante(record: $0, database: self.publicDB)
            }
            self.ristoranti.sort(by: { $0.distance(to: LocationSingleton.sharedInstance.lastLocation) < $1.distance(to: LocationSingleton.sharedInstance.lastLocation) })
            DispatchQueue.main.async{
                completion(nil)
            }
            
        }
        
    }
    
    //MARK: - Filter Function
    @objc func setAreaFilter( area: String) -> Void  {
        if(areaFilter != "" && areaFilter != area){
            areaFilter=area
        }
        else if (areaFilter != "" && areaFilter == area){
            areaFilter = ""
        }
        else {
            areaFilter = area
        }
    }
    
    @objc func setCategoryFilter( category: String) -> Void {
        if(categoryFilter != "" && categoryFilter != category){
            categoryFilter=category
        }
        else if (categoryFilter != "" && categoryFilter == category){
            categoryFilter = ""
        }
        else {
            categoryFilter = category
        }
        
    }
    
    func getCategoryFilter() -> String {
        return self.categoryFilter
    }
    
    func getAreaFilter() -> String {
        return self.areaFilter
    }
    
}
