//
//  NewViewController.swift
//  PageControl
//
//  Created by Christian Varriale on 18/03/2020.
//  Copyright © 2020 Seemu. All rights reserved.
//

import UIKit

class ThirdPageViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button?.alpha = 0.0
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1.0, delay: 0.5,
                               options: [],
                               animations: {
        self.button?.alpha = 1.0
            }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.0,
                               options: [],
                               animations: {
        self.button?.alpha = 0.0
            }, completion: nil)
    }

    @IBAction func gotTapped(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        UserDefaults.standard.set(true, forKey: "OnBoardingComplete")
        
        userDefaults.synchronize()
        
        let storyboard = UIStoryboard(name: "App", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "App")
        self.present(controller, animated: false, completion: nil)
    }
    
}
